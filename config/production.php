<?php
/**
 * @copyright Copyright (c) 2017 Matthias Walter
 * @copyright Copyright (c) 2017 netz98 GmbH (http://www.netz98.de)
 *
 * @see LICENSE
 */

namespace Deployer;

$deployPath = '/var/www/production.shop.com';

$local = localhost('production');
$local->user('juancarlosc');
$local->set('deploy_path', $deployPath);
$local->stage('production');

$local->set('config_store_env', '__SET_YOUR_CONFIG_STORE_ENV__');
$local->set('webserver_user', '__SET_YOUR_LOCAL_USER__');
$local->set('webserver_group', '__SET_YOUR_LOCAL_GROUP__');
$local->set('bin/n98_magerun2', 'n98-magerun2');

$local->roles('web', 'db');