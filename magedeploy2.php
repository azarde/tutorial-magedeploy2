<?php

return [
    'env' => [
        'git_bin' => getenv('GIT_BIN') ?: '/usr/bin/git',
        'php_bin' => getenv('PHP_BIN') ?: '/usr/bin/php',
        'tar_bin' => getenv('TAR_BIN') ?: '/bin/tar',
        'mysql_bin' => getenv('MYSQL_BIN') ?: '/usr/bin/mysql',
        'composer_bin' => getenv('COMPOSER_BIN') ?: '/usr/local/bin/composer',
        'deployer_bin' => getenv('DEPLOYER_BIN') ?: '/usr/local/bin/deployer',
    ],
    'deploy' => [
        'git_url' => 'https://github.com/maven1206/magento2.git',
        'git_dir' => 'shop',
        'app_dir' => 'shop',
        'artifacts_dir' => 'artifacts',
        'themes' => [
            ['code' => 'Magento/luma', 'languages' => ['en_US']],
            ['code' => 'Magento/backend', 'languages' => ['en_US']]
        ],
        'artifacts' => [
            'shop.tar.gz' => ['dir' => '.','options' => ['--exclude-vcs','--checkpoint=5000']],
        ],
        'clean_dirs' => [
            'var/cache',
            'var/di',
            'var/generation',
        ],
    ],
    'build' => [
        'db' => [
            'db-host' => getenv('DB_HOST') ?: '127.0.0.1',
            'db-name' => getenv('DB_NAME') ?: 'magedeploy2_dev',
            'db-user' => getenv('DB_USER') ?: 'root',
            'db-password' => getenv('DB_PASSWORD') ?: 'q1w2e3r4',
            'admin-email' => 'juancarlosc@onetree.com',
            'admin-firstname' => 'Juan',
            'admin-lastname' => 'Conde',
            'admin-password' => 'q1w2e3r4',
            'admin-user' => 'admin',
            'backend-frontname' => 'admin',
            'base-url' => 'http://testeticket.com',
            'base-url-secure' => 'https://testeticket.com',
            'currency' => 'USD',
            'language' => 'en_US',
            'session-save' => 'files',
            'timezone' => 'America/Chicago',
            'use-rewrites' => '1',
            'use-secure' => '0',
            'use-secure-admin' => '0',
        ],
    ],
];